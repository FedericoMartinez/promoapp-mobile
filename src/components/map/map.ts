import {Component, Input} from '@angular/core';
import {Location} from "../../models/location";
import {Company} from "../../models/company";
import {NavController} from "ionic-angular";
import {CompanyDetailsPage} from "../../pages/company-details/company-details";

@Component({
  selector: 'map-component',
  templateUrl: 'map.html'
})
export class MapComponent {

  @Input() mapLocation: Location = new Location(-34.9066475, -54.9593562);
  @Input() companies: Array<Company>;

  constructor(private navigationController: NavController) {
  }

  centerChangeHandler($event): void {
    console.info("Map center change", $event);
    this.mapLocation.latitude = $event.lat;
    this.mapLocation.longitude = $event.lng;
  }

  public markerClick(company: Company) {
    this.openCompanyPage(company);
  }

  public viewCompanyDetails(company: Company) {
    this.openCompanyPage(company);
  }

  private openCompanyPage(company: Company) {
    this.navigationController.push(CompanyDetailsPage, {company: company.id});
  }

}
