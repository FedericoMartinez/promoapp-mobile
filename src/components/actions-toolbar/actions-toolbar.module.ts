import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ActionsToolbarComponent } from './actions-toolbar';

@NgModule({
  declarations: [
    ActionsToolbarComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ActionsToolbarComponent
  ]
})
export class ActionsToolbarComponentModule {}
