import { Component } from '@angular/core';

/**
 * Generated class for the ActionsToolbarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'actions-toolbar',
  templateUrl: 'actions-toolbar.html'
})
export class ActionsToolbarComponent {

  text: string;

  constructor() {
    console.log('Hello ActionsToolbarComponent Component');
    this.text = 'Hello World';
  }

}
