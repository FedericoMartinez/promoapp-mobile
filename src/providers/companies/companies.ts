import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Company} from "../../models/company";
import {Location} from "../../models/location";

/*
  Generated class for the CompaniesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CompaniesProvider {

  private companies: Array<Company> = [];

  constructor(public http: Http) {
    this.companies.push(new Company(1, "Company 1", new Location(-34.9066475, -54.9593562), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate erat. In imperdiet elit tempus metus cursus, vitae consequat. "));
    this.companies.push(new Company(2, "Company 2", new Location(-34.9166475, -54.9593562), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate erat. In imperdiet elit tempus metus cursus, vitae consequat. "));
    this.companies.push(new Company(3, "Company 3", new Location(-34.9266475, -54.9593562), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel vulputate erat. In imperdiet elit tempus metus cursus, vitae consequat. "));
  }

  public retrieveCompanies(location: Location): Promise<Array<Company>> {
    console.debug("getting companies for location", location);
    return new Promise<Array<Company>>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.companies);
      }, 1500);
    });
  }

  public retrieveCompany(id: number): Promise<Company> {
    return new Promise<Company>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.companies.find((c) => c.id == id));
      }, 1500);
    });
  }

  public sendReview(company: Company, review: string) {

  }

}
