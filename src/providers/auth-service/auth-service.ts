import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export class User {
  name: string;
  email: string;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}

/*
 Generated class for the AuthServiceProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class AuthServiceProvider {

  currentUser: User;

  constructor(public http: Http) {
  }

  public login(credentials): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (credentials.email === null || credentials.password === null) {
          reject();
        } else {
          let access = (credentials.password === "pass" && credentials.email === "email");
          this.currentUser = new User('user', 'user@example.com');
          resolve(access);
        }
      }, 1500);
    });
  }

  public register(credentials) {
    return new Promise((resolve, reject) => {
      if (credentials.email === null || credentials.email.length <= 0 ||
        credentials.password === null || credentials.password.length <= 0) {
        reject("Please insert credentials");
      } else {
        this.currentUser = new User(credentials.email, credentials.password);
        resolve(this.currentUser);
      }
    });
  }

  public getUserInfo(): User {
    return this.currentUser;
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }

}
