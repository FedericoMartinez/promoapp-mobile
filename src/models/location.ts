export class Location {
  public constructor(public latitude: number = 0, public longitude: number = 0) {
  }
}
