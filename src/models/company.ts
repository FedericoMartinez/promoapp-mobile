import {Location} from "./location";
import {Promotion} from "./promotion";

export class Company {
  public rate: number;
  public promotions: Array<Promotion> = [];

  constructor(public id: number, public name: string,
              public location: Location, public details: string) {

  }
}
