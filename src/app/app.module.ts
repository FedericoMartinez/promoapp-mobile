import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {LoginPage} from "../pages/login/login";
import {HttpModule} from "@angular/http";
import {ActionsToolbarComponent} from '../components/actions-toolbar/actions-toolbar';
import {MapComponent} from '../components/map/map';
import {AgmCoreModule} from '@agm/core';
import {GoogleMapsAPIWrapper} from '@agm/core/services/google-maps-api-wrapper';
import {Geolocation} from '@ionic-native/geolocation';
import { CompaniesProvider } from '../providers/companies/companies';
import {CompanyDetailsPage} from "../pages/company-details/company-details";
import {Ionic2RatingModule} from "ionic2-rating";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ActionsToolbarComponent,
    MapComponent,
    CompanyDetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAyh_RH2ARVy6J4Y9XHFuzKRHQqxqi0xlQ'
    }),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    CompanyDetailsPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    StatusBar,
    SplashScreen,
    GoogleMapsAPIWrapper,
    Geolocation,
    CompaniesProvider
  ]
})
export class AppModule {
}
