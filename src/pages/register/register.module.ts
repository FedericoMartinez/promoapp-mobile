import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import {HomePageModule} from "../home/home.module";
import {HomePage} from "../home/home";

@NgModule({
  declarations: [
    RegisterPage,
    HomePage
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    HomePageModule
  ],
  exports: [
    RegisterPage
  ]
})
export class RegisterPageModule {}
