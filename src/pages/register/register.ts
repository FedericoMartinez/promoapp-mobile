import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerCredentials = {email: '', password: ''};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private authProvider: AuthServiceProvider) {
  }

  ionViewDidLoad() {
  }

  public createAccount() {
    if (this.registerCredentials.email.length > 0 && this.registerCredentials.password.length > 0) {
      this.authProvider.register(this.registerCredentials).then(() => {
        this.navCtrl.push(HomePage);
      }, error => {
        this.showError(error);
      });
    } else {
      this.showError('Check input values');
    }
  }

  private showError(text: string) {
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
