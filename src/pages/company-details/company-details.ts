import {Component, OnInit} from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import {Company} from "../../models/company";
import {CompaniesProvider} from "../../providers/companies/companies";

/**
 * Generated class for the CompanyDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-company-details',
  templateUrl: 'company-details.html',
})
export class CompanyDetailsPage implements OnInit {

  private company: Company;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private companyService: CompaniesProvider) {

  }

  public ngOnInit() {
    this.companyService.retrieveCompany(this.navParams.get('company')).then((company) => {
      this.company = company;
    });
  }

  public ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyDetailsPage');
  }

  public onRatingChange($event) {
    // update company rate here
  }

}
