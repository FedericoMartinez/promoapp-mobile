import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Company} from "../../models/company";
import {CompaniesProvider} from "../../providers/companies/companies";
import {Location} from "../../models/location";
import {Geolocation} from "@ionic-native/geolocation";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {

  private mapLocation: Location = new Location(-34.9066475, -54.9593562);
  private companies: Array<Company> = [];

  constructor(private navCtrl: NavController, private companiesProvider: CompaniesProvider,
              private geolocation: Geolocation) {
    this.fetchCompanies(this.mapLocation);
  }

  ngOnInit() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.info('Currect position');
      this.mapLocation.latitude = resp.coords.latitude;
      this.mapLocation.longitude = resp.coords.longitude;
    }, (error) => {
      console.error('Error getting location', error);
    });
    this.geolocation.watchPosition().subscribe((data) => {
      console.info('updating location');
      this.mapLocation.latitude = data.coords.latitude;
      this.mapLocation.longitude = data.coords.longitude;
      this.fetchCompanies(this.mapLocation);
    }, (error) => {
      console.error('error updating location');
    });
  }

  private fetchCompanies(location: Location) {
    this.companiesProvider.retrieveCompanies(location).then((companies) => {
      this.companies = companies;
    });
  }
}
